<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>
<div class="col-md-12 bg-black">
	<div class="col-md-12">
		<div class="col-md-2 col-md-offset-5 landing-logo">
			<img src="<?= base_url().ASSETS_IMG.'logo-pos.png' ?>" width="100%"/>
		</div>
	</div>

	<div class="col-md-12">
		<div class="col-md-4 col-md-offset-4 login-box">
			<div class="alert alert-info alert-dismissable hide">
			    <i class="fa fa-info"></i>
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			    <?= $alert ?>
			</div>

			<form id="form-login" action="<?= base_url().'accounts/login_handler' ?>" method="POST">
				<div class="form-group has-feedback has-feedback-left">
					<input type="text" class="form-control" name="username" placeholder="<?= lang('username') ?>">
					<i class="fa fa-user form-control-feedback"></i>
				</div>

				<div class="form-group has-feedback has-feedback-left">
					<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
					<i class="fa fa-lock form-control-feedback"></i>
				</div>

				<div class="form-group text-center">
					<input type="submit" class="btn btn-orange" name="btn_login" value="Log In">
				</div>
			</form>
		</div>
	</div>
</div>