<ul class="nav nav-tabs">
  <li class="<?= ($menu == "report_by_dc" ? 'active' : '') ?>"><a href="<?= base_url().'reports' ?>"><?= lang('report_by_dc') ?></a></li>
  <li class="<?= ($menu == "report_by_courier" ? 'active' : '') ?>"><a href="<?= base_url().'reports/report_by_courier' ?>"><?= lang('report_by_courier') ?></a></li>
</ul>