<div class="col-md-12">
	<div class="col-md-12 shipment-filter-wrap">
		<form class="form-inline">
			<div class="form-group">
				<select name="courier_map_filter" class="form-control" id="courier-map-filter">
					<option value="-1"><?= lang('all') ?></option>
					<?php foreach ($delivery_status as $key => $value) { ?>
						<option value="<?= $key ?>" <?= ($type == $key ? 'selected' : '') ?>><?= $value ?></option>
					<?php } ?>
				</select>
				<input type="hidden" name="id" value="<?= $id ?>">
			</div>
		</form>
	</div>
	<div id="courier-map" style="width:100%;height:585px"></div>
</div>