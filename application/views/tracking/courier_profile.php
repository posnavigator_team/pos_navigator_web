<div class="col-md-12 margin-top-50">
	<div class="col-md-3 bg-dark-grey no-padding">
		<img src="<?= base_url().ASSETS_IMG.'ic-avatar.png' ?>" width="100%">
	</div>
	<div class="col-md-8 profile-desc">
		<p><b>
			<i class="fa fa-user fa-lg"></i>
			<span><?= $courier['name'] ?></span>
		</b></p>
		<p><b>
			<i class="fa fa-credit-card"></i>
			<span><?= $courier['nippos'] ?></span>
		</b></p>
		<p><b>
			<i class="fa fa-mobile fa-2x"></i>
			<span><?= $courier['hp_no'] ?></span>
		</b></p>
		<p><b>
			<i class="fa fa-home fa-lg"></i>
			<span><?= $courier['office_address'] ?></span>
		</b></p>
		<p><b>
			<i class="fa fa-phone fa-lg"></i>
			<span><?= $courier['telp_office'] ?></span>
		</b></p>
		<p><b>
			<i class="fa fa-fax fa-lg"></i>
			<span><?= $courier['fax_no'] ?></span>
		</b></p>
		<p><b>
			<i class="fa fa-users fa-lg"></i>
			<span><?= $courier['position'] ?></span>
		</b></p>
		<p><b>
			<i class="fa fa-building fa-lg"></i>
			<span><?= $courier['department'] ?></span>
		</b></p>
		<p><b>
			<i class="fa fa-qrcode fa-lg"></i>
			<span><?= $courier['bb_pin'] ?></span>
		</b></p>
		<p><b>
			<i class="fa fa-envelope fa-lg"></i>
			<span><?= $courier['email'] ?></span>
		</b></p>
	</div>
</div>