<div class="col-md-12 no-padding">
	<div class="col-md-12 shipment-filter-wrap">
		<form class="form-inline">
			<div class="form-group">
				<select name="shipment_filter" class="form-control" id="shipment-filter">
					<option value="-1"><?= lang('all') ?></option>
					<?php foreach ($delivery_status as $key => $value) { ?>
						<option value="<?= $key ?>" <?= ($type == $key ? 'selected' : '') ?>><?= $value ?></option>
					<?php } ?>
				</select>
				<input type="hidden" id="courier-id" data-id="<?= $id ?>">
			</div>
		</form>
	</div>
	<?php foreach ($orders as $key => $value) { ?>
		<div class="col-md-12 no-padding border-btm-grey order-item">
			
			<div class="col-md-12 no-padding">
				<div class="col-md-9"><b><?= $value['recipient_name'] ?></b></div>
				<?php if($value['delivery_status'] != DELIVERY_STATUS_NO_STATUS){ ?>
					<?php 
						if($value['delivery_status'] == DELIVERY_STATUS_SENT){ 
							$icon = 'fa-check-circle';
							$stat_class = 'text-status-green';
						}else if($value['delivery_status'] == DELIVERY_STATUS_NOT_SENT){
							$icon = 'fa-times-circle';
							$stat_class = 'text-status-red';
						}else{
							$icon = 'fa-user';
							$stat_class = 'text-status-orange';
						}
					?>
					<div class="col-md-3 no-padding text-right <?= $stat_class ?>"><small class="bg-light-grey delivery-status"><i class="fa <?= $icon ?>"></i> <?= $delivery_status[$value['delivery_status']] ?></small></div>
				<?php } ?>
				<div class="col-md-12"><b><?= $value['recipient_address'] ?></b></div>
				<div class="col-md-12">
					<div class="col-md-3 no-padding">
						<?php $package_ic = ($value['package_type'] == PACKAGE_TYPE_BOX ? 'ic-box.png' : 'ic-mail.png') ?>
						<img src="<?= base_url().ASSETS_IMG.$package_ic ?>" width="20px"> <span><?= $package_type[$value['package_type']] ?></span>
					</div>
					<div class="col-md-3 no-padding">
						<img src="<?= base_url().ASSETS_IMG.'ic-barcode.png' ?>" width="20px"> <span><?= $value['barcode_no'] ?></span>
					</div>
				</div>
				<?php if($value['delivery_status'] == DELIVERY_STATUS_SENT){  ?>
					<div class="col-md-12 status-field">
						<span><?= lang('delivered_date')." : ".$value['delivered_date'] ?></span>
					</div>
					<div class="col-md-12 status-field">
						<span><?= lang('receiver_name')." : ".$value['receiver_name'] ?></span>
					</div>
				<?php } ?>
				<?php if($value['delivery_status'] == DELIVERY_STATUS_NOT_SENT){  ?>
					<div class="col-md-12 status-field">
						<span><?= lang('delivered_date')." : ".$value['delivered_date'] ?></span>
					</div>
					<div class="col-md-12 status-field">
						<span><?= lang('status')." : ".$delivery_status_not_sent[$value['delivery_status_code']] ?></span>
					</div>
				<?php } ?>
			</div>
			<hr class="thin-line">
		</div>

	<?php } ?>
</div>