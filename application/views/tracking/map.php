<div class="col-md-12 no-padding">
	<div class="col-md-12 header-filter-box">
		<form class="form-inline" method="POST">
		  <div class="form-group">
		    <label><?= lang('dc') ?></label>
		    <select name="dc_id" class="form-control">
		    	<option value="0"><?= lang('all') ?></option>
		    	<?php foreach ($master_dc as $key => $value) { ?>
		    		<option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
		    	<?php } ?>
		    </select>
		  </div>
		  <button type="button" class="btn btn-primary" id="map-search"><?= lang('search') ?></button>
		</form>
	</div>
	<div id="map" style="width:100%;height:585px"></div>
</div>