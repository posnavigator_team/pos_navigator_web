<?php

class User_session extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_user() {
		return $this->session->userdata('pos_navigator_spv');
	}

	function set_user($data) {
		$this->session->set_userdata('pos_navigator_spv', $data);
	}

	function clear() {
		$this->session->sess_destroy();
	}
}