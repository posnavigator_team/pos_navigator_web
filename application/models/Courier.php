<?php
class Courier extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get("courier");
	}

	function get_join($where = NULL){
		$this->db->select('courier.*, user.nippos');
		$this->db->from('courier');
		$this->db->join('user','user.id = courier.user_id');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}
}
?>