<?php
class Delivery extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get("delivery_order");
	}

	function getTodayDeliveries($type, $lat, $lng, $user_id, $search, $status = "-1"){
		// get do
		$do = $this->get('courier_id = ' . $user_id . ' AND date = CURDATE()')->row_array();

		// get shipment = delivery destinations
		$this->load->model('shipment');
		$where = array('do_id' => $do['id']);
		if($type != NULL){
			$where['package_type'] = $type;
		}
		if($status != "-1"){
			$where['delivery_status'] =  $status;
		}
		$like = NULL;
		if($search != ""){
			$like = array('barcode_no' => $search);
		}
		$destinations = $this->shipment->get($where, $like)->result_array();
	
		// to find nearest delivery and calculate estimate ditances and times
		$waypoints = "";
		foreach ($destinations as $destination) {
			$waypoints .= "|" . $destination['recipient_lat'] . "," . $destination['recipient_lng'];
		}

		//$lat = -6.884513;
		//$lng = 107.631128;
		/*$routes = $this->getRoutes($lat,$lng,$waypoints);

		$order = $routes['order'];
		$ordered_deliveries = array();
		foreach ($order as $value) {
			$ordered_deliveries[] = $destinations[$value];
		}*/
		$ordered_deliveries = $destinations;

		$distances = 0;
		$times = 0;
		/*foreach ($routes['routes'] as $route) {
			$distances = $distances + $route['distance']['value']; // in meter
			$times = $times + $route['duration']['value']; // in second
		}*/
		return array('deliveries' => $ordered_deliveries, 'distance' => $distances, 'time' => $times);
	}

	function getRoutes($origin_lat, $origin_lng, $waypoints){
	    $url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$origin_lat.",".$origin_lng."&destination=".$origin_lat.",".$origin_lng."&waypoints=optimize:true".$waypoints."&sensor=false";
	    echo $url;
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    $result = json_decode($response, true);
	    return array('routes' => $result['routes'][0]['legs'], 'order' => $result['routes'][0]['waypoint_order']);
	}

	function getFailedDeliveries($user_id){
		$this->db->select('s.*');
		$this->db->from('delivery_order as do');
		$this->db->join('shipment as s', 's.do_id = do.id');
		$this->db->where('do.courier_id', $user_id);
		$this->db->where('s.delivery_status', DELIVERY_STATUS_NOT_SENT);
		return $this->db->get()->result_array();
	}

	function getDelivered($user_id){
		$this->db->select('s.*');
		$this->db->from('delivery_order as do');
		$this->db->join('shipment as s', 's.do_id = do.id');
		$this->db->where('do.courier_id', $user_id);
		$this->db->where('s.delivery_status', DELIVERY_STATUS_SENT);
		return $this->db->get()->result_array();
	}

	function updateShipment($id, $data){
        $this->db->where("id", $id);
        return $this->db->update("shipment", $data);
	}

	function getDelivery($where = NULL){
		$this->db->select('s.*');
		$this->db->from('delivery_order as do');
		$this->db->join('shipment as s', 's.do_id = do.id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function getDeliveryList($user_id, $where = NULL){
		$this->db->select('s.*');
		$this->db->from('delivery_order as do');
		$this->db->join('shipment as s', 's.do_id = do.id');
		$this->db->where('do.courier_id', $user_id);
		$this->db->where('do.date = CURDATE()');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get()->result_array();
	}
}