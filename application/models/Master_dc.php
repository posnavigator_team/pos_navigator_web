<?php
class Master_dc extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get("master_dc");
	}
}
?>