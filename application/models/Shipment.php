<?php
class Shipment extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL, $like = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		if($like != NULL){
			$this->db->like($like);
		}

		return $this->db->get("shipment");
	}

	function get_by_master_dc($dc_id, $where = null){
		$this->db->select('shipment.*');
		$this->db->from('shipment');
		$this->db->join('delivery_order','shipment.do_id = delivery_order.id');
		$this->db->join('courier','courier.user_id = delivery_order.courier_id');
		$this->db->join('user','courier.user_id = user.id');
		$this->db->where('user.master_dc_id', $dc_id);
		if($where != null){
			$this->db->where($where);
		}

		return $this->db->get();
	}

	function get_by_courier($courier_id, $where = null){
		$this->db->select('shipment.*');
		$this->db->from('shipment');
		$this->db->join('delivery_order','shipment.do_id = delivery_order.id');
		$this->db->join('courier','courier.user_id = delivery_order.courier_id');
		$this->db->where('courier.user_id', $courier_id);
		if($where != null){
			$this->db->where($where);
		}

		return $this->db->get();
	}
}
?>