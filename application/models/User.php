<?php
class User extends CI_Model {

	function __construct() {
		parent::__construct();		
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get("user");
	}

	function add($data){
		$this->load->library('hash');
		if($data['password'] != ""){
			$data['password'] = $this->hash->hash_password($data['password']);
		}
		$this->db->insert('user',$data);
		return $this->db->insert_id();
	}

	function edit($id, $data){
		$this->load->library('hash');
		if($data['password'] != ""){
			$data['password'] = $this->hash->hash_password($data['password']);
		}
		$this->db->where('id', $id);
		return $this->db->update('user',$data);
	}

	function auth($username, $password) {
		$retval = array();
		$this->db->select('user.id, user.nippos, user.password, user.email, user.master_dc_id, courier.name, courier.hp_no, courier.office_address');
		$this->db->from('user');
		$this->db->join('courier', 'courier.user_id = user.id');
		$this->db->where('nippos', $username);
		$this->db->where('role_id', USER_ROLE_COURIER);
		$this->db->where('active_status', STATUS_ACTIVE);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$user = $query->row_array();
			
			if ($this->check_password($password, $user['password'])) {
				unset($user['password']);			
				$retval = $user;
			}
		}
		
		return $retval;
	}

	function check_password($password, $hash) {
		$this->load->library('hash');
		return $this->hash->check_password($password, $hash);
	}

	function auth_spv($username, $password) {
		$retval = array();
		$this->db->select('user.id, user.nippos, user.password, user.email, user.master_dc_id, spv.name');
		$this->db->from('user');
		$this->db->join('supervisor as spv', 'spv.user_id = user.id');
		$this->db->where('nippos', $username);
		$this->db->where('role_id', USER_ROLE_SUPERVISOR);
		$this->db->where('active_status', STATUS_ACTIVE);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			$user = $query->row_array();
			
			if ($this->check_password($password, $user['password'])) {
				unset($user['password']);			
				$retval = $user;
			}
		}
		
		return $retval;
	}
}
?>