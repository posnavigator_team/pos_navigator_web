<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class API_Controller extends CI_Controller {

	protected $user_id = null;

	function __construct() {
		parent::__construct();
		$this->load->model('auth_token');
		$this->layout = false;
        $this->res_noparam = array(
        	'status' => 0,
        	'message'=> 'Parameter not found'
        	);
        $this->res_nodata = array(
        	'status'=> 0,
        	'message'=> 'Data not found'
        	);
	}

	protected function response($data, $http_code = 200) {
		header('HTTP/1.1: ' . $http_code);
		header('Status: ' . $http_code);
		header('Content-Type: application/json');

		exit(json_encode($data));
	}

	protected function check_auth_token() {
		$headers = $this->input->request_headers();
		$string = explode(' ',$headers['Authorization']);
		
		if(count($string) == 2){
			$token = $string[1];
		}
		
		$this->user_id = $this->auth_token->check_auth_token($token);

		if(!$this->user_id) {
			$this->response(array(
				'status' => -1,
				'message' => 'Invalid access token.'
			), 401);
		}
		
	}
        
    protected function check_auth_token_on_json() {
            $post_data = json_decode(file_get_contents('php://input'), true);
            $post_data['auth_token'] = isset($post_data['auth_token'])
                    ? $post_data['auth_token']
                    : -1;
            $this->user_id = $this->auth_token->check_auth_token($post_data['auth_token']);
            if(!$this->user_id){
                    $this->response(array(
			'status' => -1,
			'message' => 'Invalid access token.'
		), 401);
            }else{
	}
            
            return $post_data;
    }
    protected function check_empty_post(){
		if(!$this->input->post()){
			$this->response($this->res_noparam);
		}
    }
}

