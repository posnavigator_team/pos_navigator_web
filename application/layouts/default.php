<?php
    $ol_user = $this->user_session->get_user();
?>
<html>
	<head>
		<base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | Pos Navigator</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>style.css" rel="stylesheet" type="text/css" />
        {{styles}}

        <!-- jQuery -->
        <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
        <?php if($this->menu == "tracking"){ ?>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPBb4c1oSUpC5hNt2axqOoZ_e2-66M5S8"></script>
        <?php } ?>
        <script type="text/javascript">
            var baseUrl = "<?=base_url()?>";
        </script>
        {{scripts}}

	</head>
	<body class="container-fluid <?= ($ol_user ? '' : 'landing-bg') ?>">            
		<div class="row content bg-black">
        <?php if($ol_user){ ?>
            <div class="col-md-1 no-padding">
                <ul class="nav nav-pills nav-stacked nav-black">
                    <li class="no-hover">
                        <a href="<?= base_url() ?>">
                            <img src="<?= base_url().ASSETS_IMG.'logo-pos.png' ?>" width="100%">
                        </a>
                    </li>
                  <li class="<?= ($this->menu == "tracking" ? 'active' : '') ?>">
                        <a href="<?= base_url() ?>">
                            <img src="<?= base_url().ASSETS_IMG.'ic-tracking.png' ?>" width="50%">
                            <span>Tracking</span>
                        </a>
                    </li>
                  <li>
                        <a href="<?= base_url() ?>">
                            <img src="<?= base_url().ASSETS_IMG.'ic-task.png' ?>" width="50%">
                            <br/><span>Tasks</span>
                        </a>
                  </li>
                  <li class="<?= ($this->menu == "report" ? 'active' : '') ?>">
                        <a href="<?= base_url().'reports' ?>">
                            <img src="<?= base_url().ASSETS_IMG.'ic-report.png' ?>" width="50%">
                            <span>Reports</span>
                        </a>
                  </li>
                  <li>
                        <a href="<?= base_url().'accounts/logout' ?>">
                            <img src="<?= base_url().ASSETS_IMG.'ic-logout.png' ?>" width="40%">
                            <br/><span>Logout</span>
                        </a>
                  </li>
                </ul>
            </div>
        <?php } ?>
            <div class="col-md-<?= ($ol_user ? '11' : '12') ?> no-padding bg-white">
	           {{content}}
            </div>
	    </div>     
	    
	     
	</body>
</html>