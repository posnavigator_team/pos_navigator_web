<?php
    $ol_user = $this->user_session->get_user();
?>
<html>
	<head>
		<base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | Pos Navigator</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>style.css" rel="stylesheet" type="text/css" />
        {{styles}}

        <!-- jQuery -->
        <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
        <?php if($this->menu == "tracking"){ ?>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPBb4c1oSUpC5hNt2axqOoZ_e2-66M5S8"></script>
        <?php } ?>
        <script type="text/javascript">
            var baseUrl = "<?=base_url()?>";
        </script>
        {{scripts}}

	</head>
	<body class="container-fluid <?= ($ol_user ? '' : 'landing-bg') ?>">            
		<div class="row content bg-light-grey">
        <?php if($ol_user){ ?>
            <div class="col-md-12 no-padding">
                <ul class="nav nav-tabs nav-tabs-center nav-black">
                    <li>
                        <a href="<?= base_url().'tracking/courier_profile/'.(isset($id) ? $id : '') ?>" class="text-orange">
                            <img src="<?= base_url().ASSETS_IMG.'ic-profile.png' ?>" class="ic-detail-menu"><?= lang('profile') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url().'tracking/courier_map/'.(isset($id) ? $id : '') ?>" class="text-orange">
                            <img src="<?= base_url().ASSETS_IMG.'ic-map.png' ?>" class="ic-detail-menu"><?= lang('map') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url().'tracking/courier_order/'.(isset($id) ? $id : '') ?>" class="text-orange">
                            <img src="<?= base_url().ASSETS_IMG.'ic-list.png' ?>" class="ic-detail-menu"><?= lang('list') ?>
                        </a>
                    </li>
                </ul>
            </div>
        <?php } ?>
            <div class="col-md-6 col-md-offset-3 no-padding padding-btm-50 bg-white">
	           {{content}}
            </div>
	    </div>     
	    
	     
	</body>
</html>