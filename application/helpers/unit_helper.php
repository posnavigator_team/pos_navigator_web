<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* Return appropriate unit for distance - $value is meter unit */
if ( ! function_exists('distance_unit'))
{
	function distance_unit($value)
	{
		if($value >= 500){
			$str = round($value / 1000)  . " KM";
		}else{
			$str = $value . " M";
		}
		return $str;
	}
}

/* Return appropriate unit for time - $value is second unit */
if ( ! function_exists('distance_time'))
{
	function distance_time($value)
	{
		if($value >= 1800){
			$str = round($value / 3600)  . " JAM";
		}else{
			$str = round($value / 60)  . " MENIT";
		}
		return $str;
	}
}
