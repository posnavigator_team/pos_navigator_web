<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*PATH DIR*/
define('ASSETS',    'assets/');
define('ASSETS_CSS',    'assets/css/');
define('ASSETS_JS',    'assets/js/');
define('ASSETS_IMG', 'assets/img/');

define('PATH_TO_ADMIN', 'administrator/');

define('OPAQUE', 0x0c32fd0b);

define('PACKAGE_TYPE', serialize(array(
	1=>'Surat / Dokumen',
	2=>'Paket'
)));

define('PACKAGE_TYPE_DOC', 1);
define('PACKAGE_TYPE_BOX', 2);

define('STATUS_ACTIVE', 1);
define('STATUS_NON_ACTIVE', 0);

define('DELIVERY_STATUS_NO_STATUS', 0);
define('DELIVERY_STATUS_CURRENT_DELIVERY', 1);
define('DELIVERY_STATUS_SENT', 2);
define('DELIVERY_STATUS_NOT_SENT', 3);

define('DELIVERY_STATUS', serialize(array(
	0=>'Pengiriman Belum Diproses',
	1=>'Pengiriman Saat Ini',
	2=>'Pengiriman Berhasil',
	3=>'Pengiriman Gagal',
)));

define('DELIVERY_STATUS_SENT_ARR', serialize(array(
	0=>"Diri Sendiri", 
	1=>"Sekretaris/Resepsionis",
	2=>"Rekan Kerja/Pegawai",
	3=>"Satpam",
	4=>"Mail Room",
	5=>"Orang serumah",
	6=>"Pembantu",
	7=>"Pemilik Kos/Asrama",
	8=>"Suami/Istri",
	9=>"Yang diberikuasa",
	10=>"Petugas P.O BOX",
	11=>"Pengirim",
)));

define('DELIVERY_STATUS_NOT_SENT_ARR', serialize(array(
	0=>"Antar Ulang", 
	1=>"Dipanggil",
	2=>"Gagal Antar",
	3=>"Gagal Antar Lokal"
)));

define('PATH_UPLOAD_TO_SIGNATURE_ATTACHMENT',    realpath(APPPATH . '../assets/attachment/signature'));

define('DEFAULT_LAYOUT','default');
define('DETAIL_LAYOUT','detail');

define('LANGUAGE_INDONESIAN','indonesian');
define('LANGUAGE_ENGLISH','english');

define('USER_ROLE_COURIER',0);
define('USER_ROLE_SUPERVISOR',1);

define('REPORT_FILTER', serialize(array(
	1=>'Harian',
	2=>'Mingguan',
	3=>'Bulanan',
	4=>'Tahunan'
)));

define('REPORT_FILTER_HARIAN', 1);
define('REPORT_FILTER_MINGGUAN', 2);
define('REPORT_FILTER_BULANAN', 3);
define('REPORT_FILTER_TAHUNAN', 4);