<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');

class Reports extends Common {

    function __construct() {
        parent::__construct();

        $this->title = "Report";
        $this->menu = "report";

        $this->lang->load('report', $this->language);

        $this->scripts[] = 'plugins/highcharts/highcharts';
        $this->scripts[] = 'site/report';
    }

    public function index() {
        $this->report_by_dc();
    }

    function report_by_dc(){
        $this->load->model('master_dc');
        $data['master_dc'] = $this->master_dc->get()->result_array();
        $data['filter'] = unserialize(REPORT_FILTER);
        $this->load->view('report/report_by_dc', $data);
    }

    function report_by_dc_handler(){
        $this->layout = FALSE;
        $this->load->model(array('master_dc','shipment'));
        $postdata = $this->postdata();
        
        $dc_delivered = array();
        $dc_not_delivered = array();

        if($postdata['dc_name'] != 0){
            $cat_list = array();
            if($postdata['filter'] == REPORT_FILTER_HARIAN){
                $cat_list = $this->get_days_in_week();
                foreach ($cat_list as $key => $value) {
                    $dc_delivered[] = $this->shipment->get_by_master_dc($postdata['dc_name'],array('shipment.delivery_status' => DELIVERY_STATUS_SENT, 'delivery_order.date' => $value))->num_rows();
                    $dc_not_delivered[] = $this->shipment->get_by_master_dc($postdata['dc_name'], array('shipment.delivery_status' => DELIVERY_STATUS_NOT_SENT, 'delivery_order.date' => $value))->num_rows();
                }
            }

            $categories = $cat_list;
        }else{
            $master_dc = $this->master_dc->get()->result_array();
            $dc_list = array();
            foreach ($master_dc as $key => $value) {
                $dc_list[] = $value['name'];
                $dc_delivered[] = $this->shipment->get_by_master_dc($value['id'],array('shipment.delivery_status' => DELIVERY_STATUS_SENT))->num_rows();
                $dc_not_delivered[] = $this->shipment->get_by_master_dc($value['id'], array('shipment.delivery_status' => DELIVERY_STATUS_NOT_SENT))->num_rows();
            }
            $categories = $dc_list;
        }

        $series = array(array('name' => "Berhasil Antar", 'data' => $dc_delivered), array('name' => "Gagal Antar", 'data' => $dc_not_delivered));
        $data = array('categories' => $categories, 'series' => $series);
        echo json_encode($data);
    }

    function report_by_courier(){
        $this->load->model('courier');
        $data['courier'] = $this->courier->get()->result_array();
        $data['filter'] = unserialize(REPORT_FILTER);
        $this->load->view('report/report_by_courier',$data);   
    }

    function report_by_courier_handler(){
        $this->layout = FALSE;
        $this->load->model(array('courier','shipment'));
        $courier = $this->courier->get()->result_array();
        $courier_list = array();
        $courier_delivered = array();
        $courier_not_delivered = array();

        foreach ($courier as $key => $value) {
            $courier_list[] = $value['name'];
            $courier_delivered[] = $this->shipment->get_by_courier($value['id'],array('shipment.delivery_status' => DELIVERY_STATUS_SENT))->num_rows();
            $courier_not_delivered[] = $this->shipment->get_by_courier($value['id'], array('shipment.delivery_status' => DELIVERY_STATUS_NOT_SENT))->num_rows();
        }

        $series = array(array('name' => "Berhasil Antar", 'data' => $courier_delivered), array('name' => "Gagal Antar", 'data' => $courier_not_delivered));
        $data = array('categories' => $courier_list, 'series' => $series);
        echo json_encode($data);   
    }

    private function postdata() {
        if ($post = $this->input->post()) {
            return $post;
        }
    }

    private function get_days_in_week(){
        $dates = array();
        $monday = date("Y-m-d",strtotime("monday this week")); 
        for ($i = 0; $i <= 6; $i++) {
            $raw_date = strtotime('+' . $i . ' days', strtotime($monday));
            $dates[] = date('Y-m-d', $raw_date);
        }

        return $dates;
    }
}
