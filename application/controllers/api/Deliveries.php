<?php

require_once(APPPATH . '/libraries/API_Controller.php');

class Deliveries extends API_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('delivery');
		//$this->user_id = 1;
	}

	function get_today_deliveries() {
		$this->check_auth_token(); 
		$this->load->model('shipment');
		$this->load->helper('unit');

		$type = $this->input->post('package_type');
		$search = $this->input->post('search');
		$lat = $this->input->post('lat') ?: 10;
		$lng = $this->input->post('lng') ?: 10;

		if($type != 0){
			$result = $this->delivery->getTodayDeliveries($type, $lat, $lng, $this->user_id, $search);
		}else{
			$result = $this->delivery->getTodayDeliveries(NULL, $lat, $lng, $this->user_id, $search);
		}

		$deliveries = $result['deliveries'];
		if(count($deliveries) > 0){
			$where = 'do_id = ' . $deliveries[0]['do_id'] . ' AND (delivery_status = '.DELIVERY_STATUS_SENT.' OR delivery_status = '.DELIVERY_STATUS_NOT_SENT.')';
			if($this->shipment->get($where)->num_rows() <= 0){
				$deliveries[0]['delivery_status'] = DELIVERY_STATUS_CURRENT_DELIVERY; // set current delivery is the first row
			}
		}

		if(count($deliveries) > 0) {
			$result = array(	
				'status' => 1,
				'data' => $deliveries,
				'est_distance' => distance_unit($result['distance']),
				'est_time' => distance_time($result['time'])
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => "Tidak ada pengiriman"
			);
		}

		$this->response($result);
	}

	function get_failed_deliveries() {
		$this->check_auth_token(); 
		
		$deliveries = $this->delivery->getFailedDeliveries($this->user_id);

		if(count($deliveries) > 0) {
			$result = array(	
				'status' => 1,
				'data' => $deliveries
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => "Tidak ada pengiriman"
			);
		}

		$this->response($result);
	}

	function get_delivered_list() {
		$this->check_auth_token(); 
		
		$deliveries = $this->delivery->getDelivered($this->user_id);

		if(count($deliveries) > 0) {
			$result = array(	
				'status' => 1,
				'data' => $deliveries
			);
		} else {
			$result = array(
				'status' => 0,
				'msg' => "Tidak ada pengiriman"
			);
		}

		$this->response($result);
	}

	function update_status(){
		$this->check_auth_token();
	//	$data = json_decode(file_get_contents('php://input'), true);
		
		$server_id = $this->input->post('id');
		/*$server_id = 6;
		$data = array("delivered_lat" => -6.884513, "delivered_long" => 107.631128,
			"delivery_status" => DELIVERY_STATUS_NOT_SENT, "delivery_status_code" => 0);*/

		$data = array("receiver_name" => $this->input->post('name'),
					  "delivery_status" => $this->input->post('status'),
					  "delivery_status_code" => $this->input->post('status_code'),
					  "delivered_date" => $this->input->post('delivered_date'),
					  "delivered_lat" => $this->input->post('delivered_lat'),
					  "delivered_long" => $this->input->post('delivered_long'),
					  "updated_at" => $this->input->post('updated_at'),
					  );

		if($server_id > 0){
			//edit
			$id = $server_id;
			if($this->delivery->updateShipment($server_id, $data)){
				if($data['delivery_status'] == DELIVERY_STATUS_SENT){
					if ($_FILES["file"]["error"] > 0) {
			            $this->response(array(
			                'status' => 0,
			                'message' => $_FILES["file"]["error"],
			            ));
			        }else {
		           		$file_name = $this->input->post('signature_filename');
		           		$this->delivery->updateShipment($id, array('signature_filename' => $file_name));
		           		$dest_file = PATH_UPLOAD_TO_SIGNATURE_ATTACHMENT . DIRECTORY_SEPARATOR . $file_name;
		            	move_uploaded_file($_FILES["file"]["tmp_name"], $dest_file);
		            }
		        }

		        $next_delivery = $this->delivery->getTodayDeliveries(NULL,$data['delivered_lat'], $data['delivered_long'], $this->user_id, "", DELIVERY_STATUS_NO_STATUS);
		        if(count($next_delivery) > 0){
		        	$this->delivery->updateShipment($next_delivery['deliveries'][0]['id'], array('delivery_status' => DELIVERY_STATUS_CURRENT_DELIVERY));
		        }
     
				$data = $this->delivery->getDelivery('s.id = '.$server_id)->row_array();
				$result = array(	
					'status' => 1,
					'msg' => 'Status has been successfully updated',
					'data' => $data
				);
			}else{
				$result = array(
					'status' => 0,
					'msg' => 'Something error occurred in the server. Your data cannot be updated at this time. Please try again later.'
				);
			}

			$this->response($result);
		}
	}
}
