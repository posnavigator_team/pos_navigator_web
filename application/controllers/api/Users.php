<?php

require_once(APPPATH . '/libraries/API_Controller.php');

class Users extends API_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user');
	}

	function login() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$last_token = $this->input->post('last_token');
		$response = $this->auth($username,$password, $last_token);	
		$this->response($response);
	}

	private function auth($username,$password, $last_token = NULL){
		if ($user = $this->user->auth($username, $password)) {
			$auth_token = $this->auth_token->generate_auth_token($user['id']);
			
			if ($last_token != NULL) {
				$this->auth_token->delete_auth_token($user['id'], $auth_token); // delete old token
			}
			
			$response = array(
				'status' => 1,
				'data' => array('user' => $user, 'auth_token' => $auth_token),
				);
		}else{								
			$error_message = "";

			$user_existed = $this->user->get(array('nippos' => $username, 'active_status' => STATUS_ACTIVE))->num_rows();
			
			if ($user_existed == 0) {
				$error_message = "The username you entered doesn't appear to belong to an account. Please check your email and try again.";
			} else {
				$error_message = "The password you entered is incorrect. Please try again.";
			}

			$response = array(
				'status' => 0,
				'msg' => $error_message
			);	
		}

		return $response;
	}

	function generate_password($password){
		$this->load->library('hash');
		echo $this->hash->hash_password($password);
	}

}
