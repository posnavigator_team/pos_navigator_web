<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');

class Dashboard extends Common {

    function __construct() {
        parent::__construct();

        $this->title = "Dashboard";
        $this->menu = "dashboard";
        $this->load->model(array('courier','master_dc'));

        //$this->lang->load('tracking', $this->language);

        //$this->scripts[] = 'site/dashboard';
    }

    public function index() {
        $this->load->view('site/dashboard');
    }

}
