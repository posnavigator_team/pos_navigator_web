<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');

class Accounts extends Common {

    function __construct() {
        parent::__construct("account");

        $this->load->model('user');

        $this->lang->load('account', $this->language);

        $this->scripts[] = 'site/account';
    }

    public function index() {
        $this->user = $this->user_session->get_user();
        if ($this->user) {
            redirect(base_url() . 'tracking');
        } else {
            $this->title = "Home";
            $data['alert'] = $this->session->flashdata('alert');
            $this->load->view('site/index', $data);
        }
    }

    function login_handler(){
        $this->layout = FALSE;
        $postdata = $this->postdata();

        $this->auth($postdata['username'], $postdata['password']);
    }
    
    private function auth($username, $password) {
            $response = array();

            $user = $this->user->auth_spv($username, $password);

            if ($user) {
                $this->user_session->set_user($user);
                redirect(base_url(). 'tracking');
            } else {
                $this->session->set_flashdata('alert', 'The username or password you entered is incorrect.');
                redirect(base_url());
            }
    }

    public function logout() {
            $this->user_session->clear();
            redirect(base_url());
    }

    private function postdata() {
            if ($post = $this->input->post()) {
                    return $post;
            }
            redirect('accounts');
    }

}
