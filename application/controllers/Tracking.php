<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');

class Tracking extends Common {

    function __construct() {
        parent::__construct();

        $this->title = "Tracking";
        $this->menu = "tracking";
        $this->load->model(array('courier','master_dc'));

        $this->lang->load('tracking', $this->language);

        $this->scripts[] = 'jquery.googlemap';
        
    }

    public function index() {
        $this->scripts[] = 'site/tracking';
        $data['master_dc'] = $this->master_dc->get()->result_array();
        $this->load->view('tracking/map', $data);
    }

    function get_marker_handler(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        $where = array("user.active_status" => STATUS_ACTIVE);
        if($postdata['dc_id'] != 0){
            $where["user.master_dc_id"] = $postdata['dc_id'];
        }
        $data['courier'] = $this->courier->get_join($where)->result_array();

        echo json_encode($data);
    }

    private function postdata() {
        if ($post = $this->input->post()) {
            return $post;
        }
    }

    function courier_profile($id){
        $this->layout = DETAIL_LAYOUT;

        $data['id'] = $id;
        $data['courier'] = $this->courier->get_join(array('courier.user_id' => $id))->row_array();
        $this->load->view('tracking/courier_profile',$data);
    }

    function courier_map($id, $type = "-1"){
        $this->layout = DETAIL_LAYOUT;
        $this->scripts[] = 'site/tracking_courier_detail';

        $data['id'] = $id;
        $data['delivery_status'] = unserialize(DELIVERY_STATUS);
        $data['type'] = $type;
        
        $this->load->view('tracking/courier_map',$data);
    }

    function get_shipment_marker(){
        $this->layout = FALSE;
        $this->load->model('delivery');
        $id = $this->input->post('id');
        $type = $this->input->post('type');

        $where = array();
        $data['type'] = $type;
        if($type != "-1"){
            $where['s.delivery_status'] = $type;
        }
        $orders = $this->delivery->getDeliveryList($id, $where);
        foreach ($orders as $key => $value) {
            if($value['package_type'] == PACKAGE_TYPE_DOC){
                $type = 'doc';
            }else if($value['package_type'] == PACKAGE_TYPE_BOX){
                $type = 'box';
            }

            if($value['delivery_status'] == DELIVERY_STATUS_NO_STATUS){
                $orders[$key]['icon'] = "ic-no-status-".$type.".png";
            }else if($value['delivery_status'] == DELIVERY_STATUS_CURRENT_DELIVERY){
                $orders[$key]['icon'] = "ic-current-delivery-".$type.".png";
            }else if($value['delivery_status'] == DELIVERY_STATUS_SENT){
                $orders[$key]['icon'] = "ic-delivery-sent-".$type.".png";
            }else if($value['delivery_status'] == DELIVERY_STATUS_NOT_SENT){
                $orders[$key]['icon'] = "ic-delivery-not-sent-".$type.".png";
            }
        }
        $data['orders'] = $orders;

        echo json_encode($data);
    }

    function courier_order($id, $type = "-1"){
        $this->layout = DETAIL_LAYOUT;
        $this->scripts[] = 'site/tracking_courier_detail';
        $this->load->model('delivery');
        $where = array();

        $data['id'] = $id;
        $data['type'] = $type;
        if($type != "-1"){
            $where['s.delivery_status'] = $type;
        }
        $data['delivery_status'] = unserialize(DELIVERY_STATUS);
        $data['delivery_status_not_sent'] = unserialize(DELIVERY_STATUS_NOT_SENT_ARR);
        $data['package_type'] = unserialize(PACKAGE_TYPE);
        $data['orders'] = $this->delivery->getDeliveryList($id, $where);
        $this->load->view('tracking/courier_order',$data);
    }

}
