var mapContainer = "#courier-map",
	mapSettings = {
      zoom: 15, // Initial zoom level (optional)
      type: "ROADMAP" // Map type (optional)
    };

$(function() {
    initFilterShipment();
    initMaps();
    initFilterMap();
});

function initFilterShipment(){
	$('#shipment-filter').on('change', function(){
		var type = $(this).val(),
			id = $('#courier-id').data('id');
		window.location.href = baseUrl + 'tracking/courier_order/'+id+'/'+type;
	});
}

function initMaps(){
	$(mapContainer).googleMap(mapSettings);

    initMarker();
}

function initMarker(){
	var data = {id : $('input[name=id]').val(), type: $('#courier-map-filter').val()};
	$.post(baseUrl+'tracking/get_shipment_marker', data, function(response){
		for (var i = 0; i < response.orders.length; i++) {
			var order = response.orders[i];
			$(mapContainer).addMarker({
		      coords: [order.recipient_lat,order.recipient_lng], // GPS coords
		      icon: baseUrl+"assets/img/"+order.icon, // Icon URL,
		    });
		}
	},'json');	
}

function initFilterMap(){
	$('#courier-map-filter').on('change', function(){
		var type = $(this).val(),
			id = $('input[name=id]').val();
		window.location.href = baseUrl + 'tracking/courier_map/'+id+'/'+type;
	});
}
