$(function(){
    var type = $('.report-content').data('id');
    if(type == "report_by_dc"){
        initReportByDc();
        $('#generate-report').on('click', function(){
            initReportByDc();
        });
    }else if(type == "report_by_courier"){
        initReportByCourier();
    }

    initFilterField();
    toogleFilterField($('.master-field'));
});

function initReportByDc(){
    var container = "#report-container",
        title = "Laporan Pengiriman berdasarkan DC",
        yAxis = "Pengiriman";
    var data = {dc_name : $('select[name=dc_name]').find('option:selected').val(), filter : $('select[name=filter]').find('option:selected').val()}

    $.post(baseUrl+'reports/report_by_dc_handler', data, function(response){
        initBasicColumnChart(container, title, response.categories, response.series, yAxis);
    },'json');


}

function initReportByCourier(){
    var container = "#report-container",
        title = "Laporan Pengiriman berdasarkan Pengirim",
        yAxis = "Pengiriman";
    $.get(baseUrl+'reports/report_by_courier_handler', "", function(response){
        initBasicColumnChart(container, title, response.categories,response.series,yAxis);
    },'json');
}

function initBasicColumnChart(container, title, dataCategories, dataSeries, yAxisLabel){
    $(container).highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        xAxis: {
            categories: dataCategories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: yAxisLabel
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: dataSeries,
        credits: {
            enabled: false
        },
    });
}

function initFilterField(){
    $('.master-field').on('change', function(){
        toogleFilterField($(this));
    });
}

function toogleFilterField(elem){
    if(elem.find('option:selected').val() == 0){
        $('.filter-field').hide();
    }else{
        $('.filter-field').show();
    }
}