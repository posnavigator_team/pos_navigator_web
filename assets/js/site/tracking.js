var mapContainer = "#map",
	mapSettings = {
      zoom: 15, // Initial zoom level (optional)
      coords: [-6.8965207,107.6339659], // Map center (optional)
      type: "ROADMAP" // Map type (optional)
    };

$(function() {
    initMaps();
    initSearch();
});

function initMaps(){
	$(mapContainer).googleMap(mapSettings);

    initMarker();
}

function initMarker(){
	var data = {dc_id : $('select[name=dc_id]').find('option:selected').val()};
	$.post(baseUrl+'tracking/get_marker_handler', data, function(response){
		for (var i = 0; i < response.courier.length; i++) {
			var courier = response.courier[i];
			$(mapContainer).addMarker({
		      coords: [courier.position_lat,courier.position_long], // GPS coords
		      icon: baseUrl+"assets/img/ic-pin-pos.png", // Icon URL,
		      title: " ", // Title
		      text:  '<div>'+
		      			'<div class="bg-dark-grey" style="width:80px;float:left;margin-bottom:10px"><img src="'+baseUrl+"assets/img/ic-avatar.png"+'" width="100%"></div>'+
		      			'<div style="float:left;margin-left:15px">'+
		      				'<div><b><a href="'+baseUrl+'tracking/courier_profile/'+courier.user_id+'" class="link-black" target="_blank">'+courier.name+'</a></b></div>'+
		      				'<div><b>'+courier.nippos+'</b></div>'+
		      				'<div><b>'+courier.hp_no+'</b></div>'+
		      			'</div>'+
		      		'</div>' // HTML content
		    });
		}
	},'json');	
}

function initSearch(){
	$('#map-search').on('click', function(){
        $(mapContainer).googleMap(mapSettings);
        initMarker();
    });
}