var formLogin = $('#form-login');

$(function () {
    initAlert();
    initValidator();
});

function initAlert() {
    if (alert != '') {
        $('.alert-info').removeClass('hide').hide().fadeIn(500, function () {
           $(this).delay(3000).fadeOut(500);
        });
    }
}

function initValidator() {
    //Form Login
    formLogin.bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                        notEmpty: {message: 'The username is required'},
                }
            },
            password: {
                validators: {
                        notEmpty: {message: 'The password is required'}
                }
            },
        }
    });
}