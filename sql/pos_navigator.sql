-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 08, 2016 at 10:15 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pos_navigator`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('d4e6ffbcb670abf7b7510533047d869f5b16e2d6', '::1', 1470629752, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437303632393532383b),
('0e158204b5b52010172087b14897dcfae41cd53d', '::1', 1470629982, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437303632393938323b);

-- --------------------------------------------------------

--
-- Table structure for table `courier`
--

CREATE TABLE IF NOT EXISTS `courier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` varchar(256) NOT NULL,
  `position` varchar(256) NOT NULL,
  `department` varchar(256) NOT NULL,
  `telp_office` varchar(20) NOT NULL,
  `fax_no` varchar(20) NOT NULL,
  `hp_no` varchar(20) NOT NULL,
  `bb_pin` varchar(20) NOT NULL,
  `email` varchar(256) NOT NULL,
  `chatting` varchar(256) NOT NULL,
  `office_address` varchar(256) NOT NULL,
  `office` varchar(256) NOT NULL,
  `updated_date` date NOT NULL,
  `region` varchar(256) NOT NULL,
  `office_code` varchar(100) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order`
--

CREATE TABLE IF NOT EXISTS `delivery_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `do_no` varchar(256) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courier_id` (`courier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shipment`
--

CREATE TABLE IF NOT EXISTS `shipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `do_id` int(11) NOT NULL,
  `barcode_no` varchar(256) NOT NULL,
  `sender_name` varchar(256) NOT NULL,
  `sender_address` varchar(256) NOT NULL,
  `sender_phone_no` int(20) NOT NULL,
  `recipient_name` varchar(256) NOT NULL,
  `recipient_address` varchar(256) NOT NULL,
  `recipient_phone_no` varchar(20) NOT NULL,
  `package_type` tinyint(2) NOT NULL,
  `delivery_status` varchar(10) NOT NULL,
  `delivery_status_code` varchar(10) NOT NULL,
  `receiver_name` varchar(256) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `do_id` (`do_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nippos` varchar(100) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role` tinyint(1) NOT NULL,
  `email` varchar(256) NOT NULL,
  `active_status` tinyint(1) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `courier`
--
ALTER TABLE `courier`
  ADD CONSTRAINT `courier_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `delivery_order`
--
ALTER TABLE `delivery_order`
  ADD CONSTRAINT `delivery_order_ibfk_1` FOREIGN KEY (`courier_id`) REFERENCES `courier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `shipment`
--
ALTER TABLE `shipment`
  ADD CONSTRAINT `shipment_ibfk_1` FOREIGN KEY (`do_id`) REFERENCES `delivery_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
